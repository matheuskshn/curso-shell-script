#!/bin/bash
#######################################################################
#                                                                     #
# BuscaAluno.sh - Procurar nome completo do aluno                     #
#                                                                     #
# Autor: Matheus Lino Gomes de Souza (matheuskshn@gmail.com)          #
# Data de Criação: 04 Set 2022                                        #
#                                                                     #
# Descrição: Script de exemplo para o comando read.                   #
#            A partir de uma entraa do usuário, será feita uma        #
#            busca no arquivo alunos2.txt                             #
#                                                                     #
# Exemplo de uso: ./BuscaAlunos.sh                                    #
#                                                                     #
#######################################################################

ARQALUNOS="/home/matheus/CursoShellScript/arquivos/alunos.txt"

clear
echo "========= Script de Busca de Alunos ========="
echo ""

# "echo -n" para não quebrar a linha.
# echo -n "Por favor, informe o nome do aluno: "
# read ALUNO
# Utilizando a opção -p do read para imprimir a mensagem na tela e amarzenar
# Oque for digitado na variável $ALUNO
read -p "Por favor, informe o nome do aluno: " ALUNO

ALUNOCOMPLETO=$(grep "$ALUNO" $ARQALUNOS)

echo ""
echo "O nome completo do aluno é: $ALUNOCOMPLETO"
echo ""
echo "Fim do Script"