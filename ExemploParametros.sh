#/bin/bash

# $0 - Nome do Programa
# $# - Quantidade de Parâmetros
# $* - Todos os parâmetros inseridos
# $1-9 - Cada um dos parêmetros

echo "O script $0 recebeu $# parêmetros"

echo "Os parâmetros recebidos foram: $*"

echo ""
echo "O parâmetro \$1 = $1"
echo "O parâmetro \$2 = $2"
echo "O parâmetro \$3 = $3"
echo "O parâmetro \$4 = $4"

#############################################################################################
# Exemplo de execução do script:                                                            
# matheus@ubuntu:~/CursoShellScript/Scripts$ ./ExemploParametros.sh Par1 Par2 Par3 Par4     
# O script ./ExemploParametros.sh recebeu 4 parâmetros                                      
# Os parâmetros recebidos foram: Par1 Par2 Par3 Par4                                        
#                                                                                           
# Parâmetro $1 = Par1                                                                       
# Parâmetro $2 = Par2                                                                       
# Parâmetro $3 = Par3                                                                       
# Parâmetro $4 = Par4                                                                       
#############################################################################################