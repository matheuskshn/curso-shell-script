#/bin/bash

# Comando 1
ls /tmp/
RETURN_CODE_CMD1=$?

# Comando 2
ls /tmp/arquivo-exemplo-que-nao-existe
RETURN_CODE_CMD2=$?

# Comando 3
ls /etc/passwd

RETURN_CODE_CMD3=$?

echo "Código de retorno do comando 1: "RETURN_CODE_CMD1=$?
echo "Código de retorno do comando 2: "RETURN_CODE_CMD2=$?
echo "Código de retorno do comando 3: "RETURN_CODE_CMD3=$?

# O comando exit vai mostrar o "exit code" de execução do script como 25:
# exit code vai de 0 a 255.
exit 25

# echo $?
# 25