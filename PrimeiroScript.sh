#!/bin/bash
#######################################################################
#                                                                     #
# PrimeiroScript.sh - Primeiro Script do Curso                        #
#                                                                     #
# Autor: Matheus Lino Gomes de Souza (matheuskshn@gmail.com)          #
#                                                                     #
# Descrição: Script que exibe data e hora atual e ler lista de alunos #
#                                                                     #
# Exemplo de uso: ./PrimeiroScript.sh                                 #
#                                                                     #
# Alteracoes:                                                         #
#           Dia - 02 Set 2022 - Criação do Script.                    #
#           Dia - 03 Set 2022 - Incluido Comentários                  #
#######################################################################

DATAHORA=$(date +%H:%M)
ARQALUNOS="/home/matheus/Cursos/ShellScript/arquivos/alunos2.txt"

# Função de Leitura da data e hora
clear
echo "========= Meu Primeiro Script ========="
echo ""
echo -n "Exibir data e hora atual: $DATAHORA"

# Area de leitura da lista de alunos
echo "======================================="
echo Listagem dos Alunos:
sort $ARQALUNOS

DATAHORA=$(date +%H)
echo "======================================="
echo Nova hora atual: $DATAHORA