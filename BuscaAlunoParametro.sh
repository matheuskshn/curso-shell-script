#!/bin/bash
#######################################################################
#                                                                     #
# BuscaAluno.sh - Procurar nome completo do aluno                     #
#                                                                     #
# Autor: Matheus Lino Gomes de Souza (matheuskshn@gmail.com)          #
# Data de Criação: 04 Set 2022                                        #
#                                                                     #
# Descrição: Script de exemplo para o uso de parametros               #
#            A partir de uma entraa do usuário, será feita uma        #
#            busca no arquivo alunos2.txt                             #
#                                                                     #
# Exemplo de uso: ./BuscaAlunosParametro.sh                           #
#                                                                     #
#######################################################################

ARQALUNOS="/home/matheus/CursoShellScript/arquivos/alunos.txt"

clear
echo "========= Script de Busca de Alunos ========="
echo ""

# grep recebe o parâmetro 1:
ALUNOCOMPLETO=$(grep "$1" $ARQALUNOS)

echo ""
echo "O nome completo do aluno é: $ALUNOCOMPLETO"
echo ""
echo "Fim do Script"

# Exemplo de execução do script: 
#############################################################################
# matheus@ubuntu:~/CursoShellScript/Scripts$ ./BuscaAlunoParametro.sh Andre
# ======= Script de Busca de Alunos ========
#
#
# O nome completo do aluno é: Andre Gonçalves
#
# Fim do Script
#############################################################################