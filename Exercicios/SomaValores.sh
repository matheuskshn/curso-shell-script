#########################################################################
# Exercício 1 - Crie um Script que após executado solicite dois valores #
# como entrada. Esses valores devem ser somados e o resultado exibido   #
# para o usuário.                                                       #
#                                                                       #
# Nome: SomaValores.sh                                                  #
#                                                                       #
# Autor: Matheus Lino Gomes de Souza (matheuskshn@gmail.com)            #
# Data de Criação: 04 Set 2022                                          #
#                                                                       #
# Descrição: O script faz a soma de dois valores inseridos              #
#            pelo usuário                                               #
#                                                                       #
# Uso: ./SomaValores.sh                                                 #
#                                                                       #
#########################################################################

clear
echo "======================================="
echo "Script para somar dois números inteiros"
echo "======================================="
echo ""
read -p "Digite o primeiro número: " NUM1
read -p "Digite o segundo número: " NUM2

RESULTADO_SOMA=$(expr $NUM1 + $NUM2)

echo "A soma de $NUM1 + $NUM2, é: $RESULTADO_SOMA"